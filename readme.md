# Embedded Systems Plotter (ESPlot) - Microcontroller-Software v0.9

## Purpose
The ESPlot-Software receives data from a real-time process running on an Embedded System such as a microcontroller and plots those data directly on an user-friendly graphical interface. The transmission of data is possible over different kind of protocols, e.g. USB or UART. Additionally, the ESPlot Software is able to send data from the computer to the Embedded System.

The configuration and the preferences of the plotting interface is initialized and performed completely on the Embedded System side.

The Software consists of two parts: the C library for the implementation of the communication protocol on an Embedded System and the corresponding computer software.

## Usage

The code of the Embedded System side is written completely in a hardware-independent way, which allows the communcation to be implemented independently from the platform used or the communication interface used. In order to run ESPlot on the Embedded System, you have to include the library of ESPlot inside your C code project. There are four files:

* comm_prot.h: Header file you have to include in your main application
* comm_prot_config.h: Header file where you can configure the preferences of the protocol
* color_def.h: Header file containing the colors definitions
* comm_prot.c: Source code of the procotol

#### Step-by-Step implementation

1. Copy the files into your project.
2. Include the comm_prot.h in your main file.
3. Edit the comm_prot_config.h accordingly to your preferences:
	* *N_TX_SIGNALS*: number of signals which should be transmitted from the Embedded System to the computer
	* *N_RX_SIGNALS*: number of signals which should be transmitted from the computer to the Embedded System
	* *MAX_COMM_BUFFER_DIM*: maximum buffer size in bytes which should be allocated for your communication interface
	* *PROCESS_FREQUENCY*: frequency of the process which sends out the data, e.g. a timer-based interrupt routine
	* *COMM_DEV_BAUDRATE*: transmission speed in baud/s of your communication interface
4. Write your own functions for *start_data_transfer* and *get_transfer_status*. Those functions are by default weak functions without any code and they will be overwritten by your provided function.
	* *start_data_transfer*: This function takes as arguments the transmission and reception buffers and their sizes from the communication procotol and gives it to your communication interface.
	* *get_transfer_status*: This function takes the flag from your communication interface whether the transfer is finished or not and gives this information to the communication protocol.
5. Create a variable from the type *comm_prot*, in this example the variable is called *USB_COMM*.
6. In your main function, call the function *comm_prot_init_struct(&USB_COMM)* once to initialize the protocol.
7. Use the functions *comm_prot_set_tx_data_info* and *comm_prot_set_rx_data_info* to define your transmission and reception signals, respectively.
	* Arguments of *comm_prot_set_tx_data_info*:
		* pointer to the *comm_prot variable*
		* index of the signal (starts from 0)
		* scaling flag, which defines if scaling with the scaling factor should be enabled (possible values: *SCALING_FACTOR_NOT_APPLIED (= 0), SCALING_FACTOR_APPLIED (= 1)*).
		* type of the variable, possible types: *TYPE_UINT8, TYPE_INT8, TYPE_UINT16, TYPE_INT16, TYPE_UINT32, TYPE_INT32, TYPE_FLOAT*
		* name of the signal (characters must be ASCII-compatible and compatible to the specifications for MATLAB workspace variables)
		* plot association: here you can assign the signal to one of the ordinary plots (*PLOT_NR_1 ... PLOT_NR_x*), a FFT plot (*PLOT_FFT_x*), a X-Y-plot (*PLOT_XY_x*) or no plot (*PLOT_NONE*). Assigning a signal to several plots is possible as well as assigning several signals to one plot (except the XY-plot).
		* scaling factor, which can scale the signal on the computer side
		* line width of the signal plot
		* color of the signal plot
	* Arguments of *comm_prot_set_rx_data_info*:
			* pointer to the *comm_prot variable*
			* index of the signal (starts from 0)
			* name of the signal (characters must be ASCII-compatible)
8. Call the function *comm_prot_init_comm(&USB_COMM)* in order to finish the initialization.
9. In your process, e.g. the timer interrupt, use the functions *comm_prot_write_tx_value* per each signal in order to provide the newest values of the signals. The function aks for a pointer to the *comm_prot*, the signal index and the actual value of the signal.
10. Call the function *comm_prot_manager(&USB_COMM)* in order to initiate the data transfer. This function will save the transmission data until the buffer is full and then transmits the data, on the other side the function receives the values from the computer.
11. Use the functions *comm_prot_get_rx_value* per each signal in order to get the newest values from the computer side. The function aks for a pointer to the *comm_prot* and the signal index and returns the actual value of the signal.
12. Use the function *comm_prot_set_cmd* to sent to the computer the command to record (RECORD_CMD) or to stop recording (NO_CMD).
13. Access to the array terminal_cmd in the comm_prot struct to access the terminal command sent by the computer.

### Example Application

The library comes with an already implemented example of the communication protocol on the STM32 microcontroller. The example can be found under /examples. It has the following features:
	* 8 transmission signals of different variable types
	* 1 reception signal for toggling a LED (0 = off, 1 = on)
	* some terminal commands for controlling a RGB LED (*LED RED, LED GREEN, LED BLUE, LED OFF*)
	* automized recordings (in 2s steps) send by the microcontroller to the computer)