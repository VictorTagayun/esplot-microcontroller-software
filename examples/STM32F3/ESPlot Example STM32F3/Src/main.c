/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"

#define USART			0
#define FT4222			1
#define COMM_INTERFACE	FT4222

#include "comm_prot.h"
#include "math.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi3;
DMA_HandleTypeDef hdma_spi3_rx;
DMA_HandleTypeDef hdma_spi3_tx;

TIM_HandleTypeDef htim5;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

// Structure of the USB Protocol
volatile comm_prot USB_COMM;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI3_Init(void);
static void MX_TIM5_Init(void);
static void MX_USART2_UART_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/**
 * Function for SPI transfer on STM32F3 based on register access (better performance than HAL library)
 */
inline void Quick_HAL_SPI_Transfer(uint32_t buffer_size, uint8_t* tx_data_buff, uint8_t* rx_data_buff)
{
	//Set pointers and size
	hspi3.ErrorCode = HAL_SPI_ERROR_NONE;
	hspi3.pTxBuffPtr = tx_data_buff;
	hspi3.pRxBuffPtr = rx_data_buff;
	hspi3.TxXferSize = buffer_size;
	hspi3.RxXferSize = buffer_size;
	hspi3.TxXferCount = buffer_size;
	hspi3.RxXferCount = buffer_size;

	//Set device to busy
	//hspi3.State = HAL_SPI_STATE_BUSY_TX_RX;

	//Enable SPI Error Interrupt
	//__HAL_SPI_ENABLE_IT(&hspi3, SPI_IT_ERR);

	//Start RX DMA Transfer
	hdma_spi3_rx.State = HAL_DMA_STATE_BUSY;
	hdma_spi3_rx.ErrorCode = HAL_DMA_ERROR_NONE;
	hdma_spi3_rx.Instance->CCR &= ~DMA_CCR_EN;
	hdma_spi3_rx.DmaBaseAddress->IFCR = (DMA_FLAG_GL1 << hdma_spi3_rx.ChannelIndex);
	hdma_spi3_rx.Instance->CNDTR = hspi3.RxXferCount;
	hdma_spi3_rx.Instance->CPAR =  (uint32_t) &hspi3.Instance->DR;
	hdma_spi3_rx.Instance->CMAR =  (uint32_t) hspi3.pRxBuffPtr;
	hdma_spi3_rx.Instance->CCR |= (DMA_IT_TC | DMA_IT_TE | DMA_CCR_EN);
	SET_BIT(hspi3.Instance->CR2, SPI_CR2_RXDMAEN);

	//Start TX DMA Transfer
	hdma_spi3_tx.State = HAL_DMA_STATE_BUSY;
	hdma_spi3_tx.ErrorCode = HAL_DMA_ERROR_NONE;
	hdma_spi3_tx.Instance->CCR &= ~DMA_CCR_EN;
	hdma_spi3_tx.DmaBaseAddress->IFCR = (DMA_FLAG_GL1 << hdma_spi3_tx.ChannelIndex);
	hdma_spi3_tx.Instance->CNDTR = hspi3.TxXferCount;
	hdma_spi3_tx.Instance->CPAR =  (uint32_t) &hspi3.Instance->DR;
	hdma_spi3_tx.Instance->CMAR =  (uint32_t) hspi3.pTxBuffPtr;
	hdma_spi3_tx.Instance->CCR |= (DMA_IT_TC | DMA_IT_TE | DMA_CCR_EN);
	SET_BIT(hspi3.Instance->CR2, SPI_CR2_TXDMAEN);
}


/**
 * Function of the protocol used for transmitting data
 */
void start_data_transfer(uint32_t buffer_size, uint8_t* tx_data_buff, uint8_t* rx_data_buff)
{

#if (COMM_INTERFACE == FT4222)
	HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*) tx_data_buff, (uint8_t*) rx_data_buff, (uint16_t) buffer_size);
	//Quick_HAL_SPI_Transfer(buffer_size, tx_data_buff, rx_data_buff);
#elif (COMM_INTERFACE == USART)
	HAL_UART_Receive_DMA(&huart2, (uint8_t*) rx_data_buff, (uint16_t) buffer_size);
	HAL_UART_Transmit_DMA(&huart2, (uint8_t*) tx_data_buff, (uint16_t) buffer_size);
#endif

}

/**
 * Function of the protocol used for getting the actual transmission status
 */
uint8_t  get_transfer_status()
{

#if (COMM_INTERFACE == FT4222)
	if (HAL_SPI_GetState(&hspi3) == HAL_SPI_STATE_READY)
		return  1;
	else
		return 0;
#elif (COMM_INTERFACE == USART)
	if (HAL_UART_GetState(&huart2) != HAL_UART_STATE_BUSY_TX)
		return  1;
	else
		return 0;
#endif

}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */


  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI3_Init();
  MX_TIM5_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  //Initialize the USB protocol
  comm_prot_init_struct(&USB_COMM);

  //Set rx signals
  comm_prot_set_rx_data_info(&USB_COMM, 0, "LED1");

  //Set tx signals
  comm_prot_set_tx_data_info(&USB_COMM, 0, SCALING_FACTOR_APPLIED, TYPE_UINT8,  "Test UINT8", 		PLOT_NR_1, 1.0, 4, BLACK);
  comm_prot_set_tx_data_info(&USB_COMM, 1, SCALING_FACTOR_APPLIED, TYPE_INT8,   "Test INT8", 			PLOT_NR_1, 2.0, 4, BLUE);
  comm_prot_set_tx_data_info(&USB_COMM, 2, SCALING_FACTOR_APPLIED, TYPE_UINT16, "Test UINT16", 		PLOT_NR_2, 3.14, 4, RED);
  comm_prot_set_tx_data_info(&USB_COMM, 3, SCALING_FACTOR_APPLIED, TYPE_INT16,  "Test INT16", 		PLOT_NR_2, 4.0, 4, RANDOM);
  comm_prot_set_tx_data_info(&USB_COMM, 4, SCALING_FACTOR_APPLIED, TYPE_UINT32, "Test UINT32", 		PLOT_NR_3, 2.0, 4, BLUE);
  comm_prot_set_tx_data_info(&USB_COMM, 5, SCALING_FACTOR_APPLIED, TYPE_INT32,  "Test INT32", 		PLOT_NR_3, 3.0, 4, RED);
  comm_prot_set_tx_data_info(&USB_COMM, 6, SCALING_FACTOR_APPLIED, TYPE_FLOAT,  "Test FLOAT", 		PLOT_NR_4, 1/3.14, 4, RANDOM);
  comm_prot_set_tx_data_info(&USB_COMM, 7, SCALING_FACTOR_NOT_APPLIED, TYPE_FLOAT, "Harmonic signal", PLOT_NR_5, 1.0, 4, BLACK);
  comm_prot_init_comm(&USB_COMM);

  //Initiate first transfer to set all the settings
  HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*) USB_COMM.tx_i_buff, (uint8_t*) USB_COMM.rx_d_buff, (uint16_t) USB_COMM.buff_dimension_info);

  //Start Timer (1 kHz Timer)
  HAL_TIM_Base_Start_IT(&htim5);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  HAL_Delay(2000);
	  comm_prot_set_cmd(&USB_COMM, RECORD_CMD);
      HAL_Delay(2000);
	  comm_prot_set_cmd(&USB_COMM, NO_CMD);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV2;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* SPI3 init function */
static void MX_SPI3_Init(void)
{

  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi3.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM5 init function */
static void MX_TIM5_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 17;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 4000;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 1000000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_8;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  huart2.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  huart2.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
  /* DMA2_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel1_IRQn);
  /* DMA2_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel2_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, LED1_Pin|LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED_R_Pin|LED_G_Pin|LED_B_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED1_Pin LED2_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_R_Pin LED_G_Pin LED_B_Pin */
  GPIO_InitStruct.Pin = LED_R_Pin|LED_G_Pin|LED_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_GPIO_Pin */
  GPIO_InitStruct.Pin = USB_GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_GPIO_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

//Signals used for plotting
uint8_t  cnt_8 = 0;
uint16_t cnt_16 = 0;
float harmonic_signal = 0.0f;

/**
 * Callback for the Main control
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	cnt_8++; //increment counter variable until it overflows
	harmonic_signal = 3.0f * sinf(2.0f*3.14f*((float)cnt_16)/1000.0f) + 0.5f * sinf(2.0f*3.14f*5.0f*((float)cnt_16)/1000.0f)+ 0.05f * sinf(2.0f*3.14f*50.0f*((float)cnt_16)/1000.0f);
	cnt_16++;
	if (cnt_16 > 999)
		cnt_16 = 0;

	//Set tx signals
	comm_prot_write_tx_value(&USB_COMM, 0, (cnt_8));
	comm_prot_write_tx_value(&USB_COMM, 1, (cnt_8 - 127));
	comm_prot_write_tx_value(&USB_COMM, 2, (cnt_8*256));
	comm_prot_write_tx_value(&USB_COMM, 3, (cnt_8-127)*256);
	comm_prot_write_tx_value(&USB_COMM, 4, (cnt_8*1000));
	comm_prot_write_tx_value(&USB_COMM, 5, (cnt_8-127)*1000);
	comm_prot_write_tx_value(&USB_COMM, 6, 3.14f*(float)cnt_8);
	comm_prot_write_tx_value(&USB_COMM, 7, harmonic_signal);

	//Update Communication
	comm_prot_manager(&USB_COMM);

	//Read rx signals
	uint8_t led = comm_prot_get_rx_value(&USB_COMM, 0);

	//Switch LED1 accordingly to the received value
	if (led == 1)
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 1);
	else
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 0);

	//Switch RGB LED accordingly to received terminal command
	char red[16] = "LED RED";
	char green[16] = "LED GREEN";
	char blue[16] = "LED BLUE";
	char off[16] = "LED OFF";
	if (strcmp(USB_COMM.terminal_cmd, red) == 0)
	{
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
	}
	else if (strcmp(USB_COMM.terminal_cmd, green) == 0)
	{
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
	}
	else if (strcmp(USB_COMM.terminal_cmd, blue) == 0)
	{
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
	}
	else if (strcmp(USB_COMM.terminal_cmd, off) == 0)
	{
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
	}

	//Switch LED2 accordingly if the connection is established or not
	if(USB_COMM.comm_state == ESTABLISHED)
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 1);
	else
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 0);

	(void) htim;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	(void) file;
	(void) line;

  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
